import unittest
import logging

logging.getLogger().setLevel(logging.INFO)

import os
import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from SimpleCalculator.SimpleCalculator_main import Calculator

class TestSimpleCalculator(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        """
        This method is executed only once at the beginning of the testing
        """
        cls.calculator = Calculator()
        logging.info("Configuring the testing paramenters")
        return super().setUpClass()


    @classmethod
    def tearDownClass(cls) -> None:
        """
        This method is executed only once at the end of the testing
        """
        del cls.calculator
        logging.info("Deleting the testing paramenters")
        return super().tearDownClass()


    def setUp(self) -> None:
        """
        This method is executed before any test is run
        """
        self.calculator.a = 4
        self.calculator.b = 5
        logging.info(f"Executing setUp() method for method {self._testMethodName}")
        return super().setUp()


    def tearDown(self) -> None:
        """
        This method is executed after any test is run
        """
        self.calculator.a = 0
        self.calculator.b = 0
        logging.info(f"Executing tearDown() method for method {self._testMethodName}")
        return super().tearDown()


    def test_CalculatorAdd(self) -> None:
        self.assertEqual(self.calculator.add(), 9)


    def test_CalculatorSub(self) -> None:
        self.assertEqual(self.calculator.sub(), -1)
    
if __name__ == "__main__":
    unittest.main()
