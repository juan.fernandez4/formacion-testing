import unittest
from unittest.mock import MagicMock, patch, Mock

import logging
import sys
import os
import inspect

logging.getLogger().setLevel(logging.INFO)

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from SimpleCalculator.SimpleCalculator_main import Calculator
import SimpleCalculator.SimpleCalculator_main

class MyTestCase(unittest.TestCase):

    # mock.patch is used to replace the default mock class to be tested
    # me need to provide the full path!
    @unittest.mock.patch('SimpleCalculator.SimpleCalculator_main.Calculator')
    def test_CalculatorMock(self, MockClass):
        instance = MockClass()

        # return valye for the method to be tested (in this case the add method)
        instance.add.return_value = 22
        logging.info(f"test_CalculatorMock: return value for add method of Calculator class: {instance.add()}")
        self.assertEqual(instance.add(), 22)

        assert MockClass is SimpleCalculator.SimpleCalculator_main.Calculator

    # The return_value is used in mocks to set the output value of the class/method
    # in this case, no matter which values are passed to add2(X,Y), the mock will assume that the 
    # return value is 99
    @unittest.mock.patch('SimpleCalculator.SimpleCalculator_main.Calculator.add2', return_value=99)
    def test_add2_mock(self, add2):
        instance_calculator = Calculator(2,3)
        logging.info(f"test_add2_mock: return value for add2 method of Calculator class: {add2(2,3)}")
        logging.info(f"test_add2_mock: return value for add method of the instantiated Calculator class: {instance_calculator.add()}")
        
        self.assertEqual(add2(3,4), 99)

        assert add2 is SimpleCalculator.SimpleCalculator_main.Calculator.add2


if __name__ == '__main__':
    unittest.main()