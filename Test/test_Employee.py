import unittest
from unittest.mock import MagicMock, patch, Mock

import logging
import sys
import os
import inspect


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from Employee.Employee_main import EmpDetails, Employee

logging.getLogger().setLevel(logging.INFO)

class TestEmployee(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.emp1 = Employee(1, 'sam', 3500)
        cls.emp2 = Employee(2, 'joey', 2200)

    def setUp(self) -> None:
        self.empDetails = EmpDetails()

    @classmethod
    def tearDownClass(cls) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_employee(self):
        sal1 = self.emp1.getSalary()
        self.assertEqual(sal1, 3500)
    
    def test_emp_details(self):
        emp = Employee()
        self.assertEqual(self.empDetails.checkTotalEmployee(), "Feeling lonely")

    @patch('Employee.Employee_main.Employee')
    def test_emp_count_with_mock(self, MockEmp):
        emp = MockEmp()
        emp.getCount.return_value = 155
        self.emp_detail = EmpDetails(emp)

        self.assertEqual(self.emp_detail.checkTotalEmployee(), "All good")